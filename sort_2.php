<?php
$filename = 'vehiculo.txt';
$file_content = file_get_contents($filename);
$lines = explode("\n", $file_content);
$datos = [];
foreach ($lines as $line) {
    $line=trim($line);
    if (!empty(trim($line))) {  
        $fields = explode('|', $line);
        $data[] = [
            'id' => trim($fields[0]),
            'dominio' => trim($fields[1]),
            'marca' => trim($fields[2]),
            'modelo' => trim($fields[3]),
            'codigo1' => trim($fields[4]),
            'codigo2' => trim($fields[5]),
        ];
    }
}
// Función de comparación para ordenar 
function compare_by_marca($a, $b) {
    return strcmp($a['marca'], $b['marca']);
}
// Ordenar los datos con el campo que seleccione , usort me permite ordenar un array con una funcion propia
usort($data, 'compare_by_marca');
// Cargo una variable con los registros ordenados, para luego guardarlos
foreach ($data as $record) {
//    echo implode('|', $record) . "\n";
    $registro_ordenado.=implode('|', $record) . "\n";
}
// guardo el contenido
file_put_contents('vehiculo_ordenado.dat', $registro_ordenado);
?>