<?php
$filename = 'personas.txt';
$file_content = file_get_contents($filename);
$lines = explode("\n", $file_content);
$datos = [];
foreach ($lines as $line) {
    $line=trim($line);
    if (!empty(trim($line))) {  
        $fields = explode('|', $line);
        $data[] = [
            'id' => trim($fields[0]),
            'lastname' => trim($fields[1]),
        ];
    }
}
// Función de comparación para ordenar 
function compare_by_lastname($a, $b) {
    return strcmp($a['lastname'], $b['lastname']);
}
// Ordenar los datos con el campo que seleccione , usort me permite ordenar un array con una funcion propia
usort($data, 'compare_by_lastname');
// Cargo una variable con los registros ordenados, para luego guardarlos
foreach ($data as $record) {
//    echo implode('|', $record) . "\n";
    $registro_ordenado.=implode('|', $record) . "\n";
}
// guardo el contenido
file_put_contents('persona_ordenada.dat', $registro_ordenado);
?>
